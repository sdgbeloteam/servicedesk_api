FORMAT: 1A

# Belo-API Docs

# Patients [/patients]
Patient resource implementation

## Patient list [GET /patients{?page,?query,?token}]
Get current patient lists

+ Parameters
    + token: (string, optional) - API Token
    + page: (integer, optional) - Pagination page parameter
        + Default: 1
    + query: (string, optional) - Search a patient by query string

+ Request (application/json)
    + Body

            {
                "page": 1,
                "token": "foobar"
            }

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "patient_id": 1,
                        "first_name": "Pia",
                        "last_name": "Wurtzbach",
                        "middle_name": "Alonzo",
                        "email": "piawurtzbach@yahoo.com",
                        "mobile": "09999999999"
                    }
                ],
                "meta": {
                    "pagination": {
                        "total": 1,
                        "count": 1,
                        "per_page": 20,
                        "current_page": 1,
                        "total_pages": 1
                    }
                }
            }

## Patient data [GET /patients/{id,?token}]
Get current patient by id

+ Parameters
    + token: (string, optional) - API Token

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "patient_id": 1,
                    "first_name": "Pia",
                    "last_name": "Wurtzbach",
                    "middle_name": "Alonzo",
                    "email": "piawurtzbach@yahoo.com",
                    "mobile": "09999999999"
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "Not Found",
                "status_code": 404
            }

# Notes [/notes]
Patient resource implementation

## Notes list [GET /notes{?page,?query,?token?patient_id}]
Get current patient lists

+ Parameters
    + token: (string, optional) - API Token
    + page: (integer, optional) - Pagination page parameter
        + Default: 1
    + query: (string, optional) - Search a patient by query string
    + patient_id: (string, required) - Filter notes by patient id

+ Request (application/json)
    + Body

            {
                "page": 1,
                "token": "foobar"
            }

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "id": 30,
                        "doctor": {
                            "id": "DOC23",
                            "name": "Dr. Pat Cordova Sauler"
                        },
                        "patient": {
                            "id": "1",
                            "first_name": "Luke",
                            "last_name": "Garces",
                            "middle_name": null,
                            "email": "lukiesluke@yahoo.com",
                            "mobile": "09989655699"
                        },
                        "timeframe": null,
                        "categories": [],
                        "created_at": "2017-03-06T18:10:57+08:00"
                    }
                ],
                "meta": {
                    "pagination": {
                        "total": 1,
                        "count": 1,
                        "per_page": 10,
                        "current_page": 1,
                        "total_pages": 1
                    }
                }
            }

## Notes data [GET /notes/{id,?token}]
Get current patient by id

+ Parameters
    + token: (string, optional) - API Token

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 1,
                    "doctor": {
                        "id": "DOC26",
                        "name": "Dr. Tina Limbag"
                    },
                    "patient": {
                        "id": "88000",
                        "first_name": " Joseph",
                        "last_name": "Gayod",
                        "middle_name": null,
                        "email": "jgayod@belomed.com",
                        "mobile": "09989664970"
                    },
                    "items": [
                        {
                            "id": 1,
                            "note_id": 1,
                            "category": "subjective",
                            "type": "text",
                            "content": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae eveniet, sequi ",
                            "created_at": "2017-03-06T15:47:27+08:00"
                        }
                    ],
                    "timeframe": null,
                    "categories": [],
                    "created_at": "2017-03-06T15:21:24+08:00"
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "Not Found",
                "status_code": 404
            }

## Notes data [POST /notes/{id,?token}]
Get current patient by id

+ Parameters
    + token: (string, optional) - API Token

+ Request (application/x-www-form-urlencoded)
    + Body

            patient_id=1&doctor_id=1&categories[]=subjective&timeframe=10

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "id": 1,
                    "doctor": {
                        "id": "DOC26",
                        "name": "Dr. Tina Limbag"
                    },
                    "patient": {
                        "id": "88000",
                        "first_name": " Joseph",
                        "last_name": "Gayod",
                        "middle_name": null,
                        "email": "jgayod@belomed.com",
                        "mobile": "09989664970"
                    },
                    "items": [],
                    "timeframe": null,
                    "categories": [],
                    "created_at": "2017-03-06T15:21:24+08:00"
                }
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "422 Unprocessable Entity",
                "errors": {
                    "patient_id": [
                        "The patient id field is required."
                    ]
                }
            }

# Patients [/patients]
Patient resource implementation

## Patient list [GET /patients{?page,?query,?token}]
Get current patient lists

+ Parameters
    + token: (string, optional) - API Token
    + page: (integer, optional) - Pagination page parameter
        + Default: 1
    + query: (string, optional) - Search a patient by query string

+ Request (application/json)
    + Body

            {
                "page": 1,
                "token": "foobar"
            }

+ Response 200 (application/json)
    + Body

            {
                "data": [
                    {
                        "patient_id": 1,
                        "first_name": "Pia",
                        "last_name": "Wurtzbach",
                        "middle_name": "Alonzo",
                        "email": "piawurtzbach@yahoo.com",
                        "mobile": "09999999999"
                    }
                ],
                "meta": {
                    "pagination": {
                        "total": 1,
                        "count": 1,
                        "per_page": 20,
                        "current_page": 1,
                        "total_pages": 1
                    }
                }
            }

## Patient data [GET /patients/{id,?token}]
Get current patient by id

+ Parameters
    + token: (string, optional) - API Token

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "patient_id": 1,
                    "first_name": "Pia",
                    "last_name": "Wurtzbach",
                    "middle_name": "Alonzo",
                    "email": "piawurtzbach@yahoo.com",
                    "mobile": "09999999999"
                }
            }

+ Response 404 (application/json)
    + Body

            {
                "message": "Not Found",
                "status_code": 404
            }