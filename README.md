# API

The api used by `belo-app IOS and Android`  applications.

### Requirements

* PHP >= 5.6
* MySQL 5.5
* php5.6-gd

### Installation

Clone the repository and install all dependencies

    git clone git@bitbucket.org/sdgbeloteam/api.git

### Branch installation

* Make sure that the `.env.example` is copied as `.env`
* Change the `JWT_SECRET` to a random alphanumeric
* Change the `APPOINTMENT_TABLE` to the desired table name
* Change the `STORAGE_HOST` to the desired configuration

### Tests

    vendor/bin/phpunit

> To run feature and units tests execute the phpunit

### Continuous Integration

By default this uses `jgayod/php5.6-mysql` image for testing. This is a debian
installation that contains specific configuration fot this system to run

### Routes Documentation


#### Medical Personnel

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
medical-personnel | GET | secured | Generates all medical personnel that is either Doctors or Aestheticians
medical-personnel?filter=Aesthetician | GET | secured | Generates all medical personnel that are Doctors
medical-personnel?filter=Doctor | GET | secured | Generates all medical personnel that are Aestheticians
medical-personnel?filter=Doctor&category=non-surgical | GET | secured | Generates all medical personnel that have non surgical category
medical-personnel?filter=Doctor&category=surgical | GET | secured | Generates all medical personnel that have surgical category
medical-personnel?query=acosta | GET | secured | Searches a medical personnel by its name

#### Branches

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
branch | GET | Public | Generates all Belo Branches
branch/{id} | GET | Public | Generates a specific branch based on ID

#### Contact Us

Endpoint | Methods Allowed | Securityt type |  Description
------------ | ------------- | ------------ | ------------
contact-us | POST | Public | Sends email to Helpdesk for Contact us inquiry form

#### My Schedule

Endpoint | Methods Allowed | Securityt type |  Description
------------ | ------------- | ------------ | ------------
my-schedule | GET | secured | Display the patients appointments
my-schedule/{id} | GET | secured | Display a specific appointment based on ID
my-schedule/{id}/appointment | PUT PATCH | secured | Update an appointment's appointment status. This will be cancelled.
my-schedule/{id}/confirmation | PUT PATCH | secured | Update an appointment's confirmation status. This will be confirmed

####  Log-in

Endpoint | Methods Allowed | Securityt type |  Description
------------ | ------------- | ------------ | ------------
auth/login | POST | secured | Login a BPC user

#### News Feed

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
news | GET | Public | Display news feed content of the App

#### Service Category

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
service-category | GET | Public | Display all service categories
service-menu/{id} | GET | Public | Display a certain service menu based on service category id

#### Patient's Profile

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
profile | GET | Secured | Display patient's details

#### Patient's Transaction

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
transaction | GET | Secured | Display patient's transaction

#### Patient's BPC points

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
points | GET | Secured | Display patient's details of BPC points

#### Signup to Belo App

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
signup | POST | Public | Signup in the Belo App

#### Alerts

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
alerts | GET | Secured | View all alerts of a Signed In Patient
alerts/{id} | GET | Secured | View a specific alert data
alerts-count | GET | Secured | Display number of unread alerts of the user


#### Social Media

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
social-media | GET | Public | display image and promotion for social media accounts

#### Booking request - Users

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
book-form-users | POST | Secured | To send booking request to helpdesk based on log in user

#### Booking request - Non Users

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
book-form | POST | Public | To send booking request to helpdesk that is  do not have a user account

#### Rebooking request

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
rebook-form-users/{id} | POST | Secured | To send a rebooking request to helpdesk based on log in user

#### Appointment Grid based on Medical Personnel

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
appointment?id={}&date={} | GET | Secured | Display all appointments of a particular medical personnel and their selected appointment date

#### Privacy Policy Contents

Endpoint | Methods Allowed | Security type |  Description
------------ | ------------- | ------------ | ------------
privacy-policy | GET | Public | Display the privacy policy data


