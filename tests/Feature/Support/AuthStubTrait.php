<?php

namespace Tests\Feature\Support;

trait AuthStubTrait
{
    public $token;

    /**
     * [setUp description]
     */
    public function setUp()
    {
        $r = $this->runApp('POST', '/api/auth/login', [
            'username' => 'pshinohara@belomed.com',
            'password' => '123456'
        ]);

        $this->token = $this->decodeResponseJson()['token'];
    }
}
