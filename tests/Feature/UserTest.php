<?php

namespace Tests\Feature;

use Tests\BaseTestCase;

class UserTest extends BaseTestCase
{
    protected $withMiddleware = false;
      
    public function test_signup()
    {
        $str = substr(md5(uniqid()), 0, 4);
        $sampleEmail = "test-".$str."@gmail.com";

        $response = $this->runApp('POST', '/public-api/signup', [
            'email' => $sampleEmail,
            'password' => '123456',
            'card_number' => 'B7C23CDE',
            'birthday' => '1993-01-04',
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function test_signup_error()
    {
        $response = $this->runApp('POST', '/public-api/signup', []);

        $this->assertEquals(422, $response->getStatusCode());
    }
}