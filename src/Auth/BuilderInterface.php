<?php

namespace ServiceDesk\Auth;

use ServiceDesk\User\User;

interface BuilderInterface
{
    /**
     * Encodes the user instance to jwt token
     * using supplied credentials
     *
     * @param  User   $user [description]
     * @return [type]       [description]
     */
    public function encode(User $user);

    /**
     * Decodes the JWT Token
     *
     * @param  User   $user [description]
     * @return [type]       [description]
     */
    public function decode($token);
}
