<?php


if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) {
            return value($default);
        }

        return $value;
    }
}

if (! function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (! function_exists('dd')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function dd()
    {
        array_map(function ($x) {
            (new \Symfony\Component\VarDumper\Dumper\HtmlDumper)-dump($x);
        }, func_get_args());

        die(1);
    }
}

if (! function_exists('module_path')) {
    /**
     * Get the url of which the module resides to
     *
     * @param  [type] $module [description]
     * @return [type]         [description]
     */
    function module_path($module, $param = null)
    {
        $settings = require __DIR__ . '/' . 'settings.php';
        $app = (new \Slim\App($settings))->getContainer();

        $services = $app->get('settings')['services'];
        $host = rtrim($services['host'], '/');
        $module = $services['modules'][$module];

        return sprintf('%s%s/%s', $host, $module, $param);
    }
}

if  (! function_exists('asset')) {

    function asset()
    {
        // This is used for testing otherwise use the condition below the application
        if (isset($_SERVER['argv']) && $_SERVER['argv'][0] === 'vendor/bin/phpunit') {
            return 'http://localhost';
        }

        list($host, $port) = explode(':', $_SERVER['HTTP_HOST']);

        $protocol = (isset($_SERVER['HTTPS'])
            || $_SERVER['SERVER_PORT'] === 443) ?
            'https' :
            'http';

        return sprintf('%s://%s:%s', $protocol, $host, $port);
    }
}

if (! function_exists('storage_path')) {
    /**
     * [storage_path description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    function storage_path($path = null, $url = null)
    {
        if($path == 'notes')
        {
            return __DIR__ . '/../storage/' . $path;
        }

        if ($path === 'uploaded_patient_pictures') {
            return __DIR__ . '/../../belo/module_nurses/' . $path . ($url ? "/$url" : null);
        }

        if ($path === 'patient-before-and-after') {
            return __DIR__ . '/../../belo/module_nurses/' . $path . ($url ? "/$url" : null);
        }

        if ($path === 'before-and-after') {
            return __DIR__ . '/../storage/belo/assets/' . $path . ($url ? "/$url" : null);
        }

        if ($path === 'faq') {
            return __DIR__ . '/../storage/belo/assets/' . $path . ($url ? "/$url" : null);
        }

        if ($path === 'promotions') {
            return __DIR__ . '/../storage/belo/assets/' . $path . ($url ? "/$url" : null);
        }

        return __DIR__ . '/../storage/belo/module_nurses/' . $path . ($url ? "/$url" : null);
    }
}

if (! function_exists('toIso8601String')) {
    /**
     * [storage_path description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    function toIso8601String($date)
    {
        return date('Y-m-d\TH:i:sO', strtotime($date));
    }
}

if (! function_exists('toAMPMFormt')) {
    /**
     * [storage_path description]
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    function toAMPMFormt($date)
    {
        return date('Y-m-d g:i a', strtotime($date));
    }
}

function generateUUID($data)
    {
        assert(strlen($data) == 16);

            $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
            $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

/**
 * create time range
 *
 * @param mixed $start start time, e.g., 7:30am or 7:30
 * @param mixed $end   end time, e.g., 8:30pm or 20:30
 * @param string $interval time intervals, 1 hour, 1 mins, 1 secs, etc.
 * @param string $format time format, e.g., 12 or 24
 */
function createTimeRange($start, $end, $interval, $format = '12') {
    $startTime = strtotime($start);
    $endTime   = strtotime($end);
    $returnTimeFormat = ($format == '12')?'Y-m-d g:i:s A':'Y-m-d G:i:s';

    $current   = time();
    $addTime   = strtotime('+'.$interval, $current);
    $diff      = $addTime - $current;

    $times = array();
    while ($startTime < $endTime) {
        $times[] = date($returnTimeFormat, $startTime);
        $startTime += $diff;
    }
    $times[] = date($returnTimeFormat, $startTime);
    return $times;
}