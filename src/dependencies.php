<?php

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['\ServiceDesk\Controllers\Controller'] = function ($c) {
    $validator = $c->get('validator');

    return new \ServiceDesk\Controllers\Controller($c, $validator);
};

$container['validator'] = function ($c) {
    $settings = $c->get('settings')['lang'];

    // Boot our validator
    $fileLoader = new \Illuminate\Translation\FileLoader(
        new Illuminate\Filesystem\Filesystem,
        $settings['lang_path']
    );

    $translator = new \Illuminate\Translation\Translator(
        $fileLoader,
        $settings['locale']
    );

    $factory = new \Illuminate\Validation\Factory($translator);
    $factory->setPresenceVerifier($c->get('validation.presence'));

    return $factory;
};

$container['validation.presence'] = function ($c) {
    return new \ServiceDesk\DatabasePresenceVerifier($c->get('db'));
};

$container['image.manager'] = function ($c) {
    $settings = $c->get('settings')['intervention'];

    return new \Intervention\Image\ImageManager($settings);
};

$container['auth'] = function ($c) {
    $settings = $c->get('settings')['jwt'];

    return new \ServiceDesk\Auth\Auth(
        new \ServiceDesk\Auth\JWTBuilder($settings),
        new \ServiceDesk\User\UserRepository,
        $c
    );
};
