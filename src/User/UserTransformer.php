<?php

namespace ServiceDesk\User;

use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * Transform the response from the database to a standard API request
     * @param  User user [description]
     * @return [type]           [description]
     */
    public function transform(User $user)
    {

        return [
            'id' => $user->id,
            'email_address' => $user->email_address,
        ];
    }
}