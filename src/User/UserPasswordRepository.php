<?php

namespace ServiceDesk\User;

use ServiceDesk\BaseRepository;
use Illuminate\Database\EloquentModel;

class UserPasswordRepository extends BaseRepository
{
     /**
     * Define the relation of this repository to the model
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Find user by id
     * @param  string $email
     * @return User
     */
    public function findUserEmail($data)
    {
        $modeldata = $this
            ->model
            ->where('email_address', $data['email'])
            ->where('account_verification', 1)
            ->first();

        return $this->validateEmail($modeldata, $data['password']);
    }

    public function validateEmail($data, $password)
    {
        /* Check email if registered */
        if($data['email_address'])
        {
            /* Continue to reset validated email.*/
            if($data['account_verification'])
            {
                $this->resetPassword($data['id'], $password);
            }

            /* Return email if it's regestered but not validated. */
            return $data['email_address'];
        }

        return false;
    }


    public function resetPassword($id, $password)
    {
    	if (isset($password)) {
            $data['password'] = $this->hash($password);
        }

        $model = $this
            ->model
            ->where('id', $id)
            ->first();

        $model->fill($data);
        $model->save();

        return $model;
    }


    /**
     * Hash the password using bcrypt
     *
     * @param  string $hassPass
     * @return encrypted and hashed password
    */
    public function hash($password, $cost = 12)
    {
        return password_hash($password, PASSWORD_BCRYPT, ['cost' => $cost]);
    }

}
