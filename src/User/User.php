<?php

namespace ServiceDesk\User;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The table associated with the model.
     * @var int
     */
    protected $table = 'app_users';

    /**
     * Indicates if the IDs are auto-incrementing.
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'patient_id',
        'password',
        'card_number',
        'email_address',
        'verification_code',
    ];

    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;

    /**
     * A getter function for accesing id
     * @return [type] [description]
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the doctors id as getter
     * @return [type] [description]
     */
    public function getPatientId()
    {
        return $this->patient_id;
    }

}
