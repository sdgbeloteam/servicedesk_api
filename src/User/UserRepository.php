<?php

namespace ServiceDesk\User;

use ServiceDesk\BaseRepository;
use Illuminate\Database\EloquentModel;

class UserRepository extends BaseRepository
{
     /**
     * Define the relation of this repository to the model
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public function model()
    {
        return User::class;
    }
     /**
     * Find user by id
     * @param  int $id
     * @return User
     */
    public function findUserById($id)
    {
        return $this
            ->model
            ->where('id', $id)
            ->first();
    }

    /**
     * [update description]
     * @param  [type] $data [description]
     * @param  [type] $id   [description]
     * @return [type]       [description]
     */
    public function update($data, $id)
    {
        if (isset($data['password'])) {
            $data['password'] = $this->hash($data['password']);
        }

        $model = $this
            ->model
            ->where('id', $id)
            ->first();

        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * Hash the password using bcrypt
     *
     * @param  string $hassPass
     * @return encrypted and hashed password
    */
    public function hash($password, $cost = 12)
    {
        return password_hash($password, PASSWORD_BCRYPT, ['cost' => $cost]);
    }

    /**
     * find the user with the [username]
     * @param  int $id [description]
     * @return [type]     [description]
     */
    public function findByUserAccess($username)
    {

        return $this
            ->model
            ->where('email_address', $username)
            ->where('account_verification', '1')
            ->first();
    }

     /**
     * find the user with the [username]
     * @param  int $id [description]
     * @return [type]     [description]
     */
    public function findByUsername($username)
    {
        return $this
            ->model
            ->where('email_address', $username)
            ->first();
    }

    public function saveRegistration($data, $patient_id)
    {
        $password = $this->hash($data['password']);

        $verification_code = time() . "-" . uniqid();

        if(!$this->findByUsername($data['email']))
        {
            return $this->model
            ->create([
                'patient_id' => $patient_id,
                'password' => $password,
                'card_number' => $data['card_number'],
                'email_address' => $data['email'],
                'verification_code' => $verification_code
             ]);
        }

        return false;
    }

    public function verifyUserRegistration($data)
    {
        $verifyData = $this->model
            ->where('card_number', $data['card_number'])
            ->where('email_address', $data['email'])
            ->where('account_verification', '1')
            ->first();

        if($verifyData){
            return 'user';
        }
    }

}
