<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\Appointment\AppointmentGridRepository;
use ServiceDesk\Appointment\AppointmentGrid;
use ServiceDesk\Appointment\AppointmentGridTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class AppointmentGridController extends Controller
{
    /**
     * [$medicalpersonnelRepository description]
     * @var [type]
     */
    protected $appointmentgridRepository;

    /**
     * Create an instance of MedicalPersonnelController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->appointmentgridRepository = new AppointmentGridRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        $modelData = $this->appointmentgridRepository->findByMedicalPersonnel(
            $request->getQueryParam('id'),
            $request->getQueryParam('date')
        );
        //dd($modelData);

        if ($modelData) {
            return $this->collection($modelData, new AppointmentGridTransformer);
        }

        return $this->errorNotFound();

    }

}