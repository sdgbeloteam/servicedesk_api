<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\ServiceCategory\ServiceCategory;
use ServiceDesk\ServiceCategory\ServiceCategoryTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ServiceCategoryController extends Controller
{
     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Filter all active News feed
        $serviceCategory = ServiceCategory::where('service_status', '1')
            ->orderBy('id', 'asc')
            ->get();

        return $this->collection($serviceCategory, new ServiceCategoryTransformer);
    }

}
