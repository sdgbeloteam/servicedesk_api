<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\MedicalPersonnel\MedicalPersonnelRepository;
use ServiceDesk\MedicalPersonnel\MedicalPersonnel;
use ServiceDesk\MedicalPersonnel\MedicalPersonnelTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class MedicalPersonnelController extends Controller
{
    /**
     * [$medicalpersonnelRepository description]
     * @var [type]
     */
    protected $medicalpersonnelRepository;

    /**
     * Create an instance of MedicalPersonnelController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->medicalpersonnelRepository = new MedicalPersonnelRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->paginator(
            $this->medicalpersonnelRepository->paginate(
                $request->getQueryParam('filter'),
                $request->getQueryParam('category'),
                $request->getQueryParam('query'),
                $request->getQueryParam('page')
        ), new MedicalPersonnelTransformer);

        return $this->errorNotFound();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $resource = $this->medicalpersonnelRepository->findByMedicalPersonnelId($args['id']);

        if ($resource) {
            return $this->item(
                $resource, new MedicalPersonnelTransformer
            );
        }

        return $this->errorNotFound();
    }


}