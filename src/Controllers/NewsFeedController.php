<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\NewsFeed\NewsFeed;
use ServiceDesk\NewsFeed\NewsFeedTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class NewsFeedController extends Controller
{
     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Filter all active News feed
        $newsFeed = NewsFeed::where('news_status', '!=', '0')
            ->orderBy('id', 'desc')
            ->get();

        return $this->collection($newsFeed, new NewsFeedTransformer);
    }

}
