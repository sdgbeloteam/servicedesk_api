<?php

namespace ServiceDesk\Controllers\Traits;

use ServiceDesk\Note\NoteItemAssessment;
use Psr\Http\Message\ResponseInterface;
use ServiceDesk\Requests\NoteItemPlanRequest;
use Psr\Http\Message\ServerRequestInterface;

trait ValidatesPlan
{
    /**
     * [validatePlan description]
     * @param  ServerRequestInterface $request [description]
     * @return [type]                          [description]
     */
    public function validatePlan(ServerRequestInterface $request)
    {
        if ($errors = $this->validate($request, new NoteItemPlanRequest))
        {
            return $errors;
        }
    }
}