<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\Appointment\AppointmentRepository;
use ServiceDesk\Appointment\Appointment;
use ServiceDesk\Appointment\AppointmentTransformer;
use ServiceDesk\Patient\Patient;
use ServiceDesk\Requests\AppointmentRequest;
use ServiceDesk\Requests\AppointmentMemberRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use ServiceDesk\MySchedule\MyScheduleRepository;
use Exception;
use ServiceDesk\Mail\Mail;
use DateTime;



class AppointmentController extends Controller
{
    /**
     * [$medicalpersonnelRepository description]
     * @var [type]
     */
    protected $appointmentRepository;

    /**
     * [$mail description]
     * @var [type]
     */
    public $mail;

    /**
     * Create an instance of MedicalPersonnelController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->appointmentRepository = new AppointmentRepository;

        $this->myscheduleRepository = new MyScheduleRepository;
    }

    /**
     * [store description]
     * @param  ServerRequestInterface $request  [description]
     * @param  ResponseInterface      $response [description]
     * @return [type]                           [description]
     */
    public function store(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($errors = $this->validate($request, new AppointmentRequest)) {
            return $errors;
        }

        $data = $request->getParsedBody();

        $finalDate = str_replace("/", "-", $data['start_date']);
        $preferredDate = $finalDate.' '.$data['start_time'];
        $displayDate = date("m-d-Y", strtotime($finalDate)).' at '.date("h:i:sa", strtotime($data['start_time']));
        $data['start_at'] = date("Y-m-d H:i", strtotime($preferredDate));

        $resource = $this
            ->appointmentRepository
            ->save($data);

        $title = 'Belo App - Appointment Request - '. $data['first_name'].' '.$data['last_name'];
        $email = $data['email'];
        $description = "{$data['branch']} {$displayDate}";
        $sender = "{$data['first_name']} {$data['last_name']}";
        $mobile = $data['contact_number'];
        $sex = $data['gender'];
        $origin = ($data['source'] == 1) ? 'IOS' : 'Android';

        try {
            $mail = new Mail();
            $resource = $mail->sendAppointmentRequest($title, $data['notes'], $email, $description, $sender, $mobile, $sex, $origin);
            return $this->jsonResponse(['message' => 'Message Sent']);
        } catch (Exception $e) {
            $mail->stopMail();

            return $this->errorInternalServerError($e->getMessage());
        }

        return $this->jsonResponse(['message' => 'Booking request sent']);
    }

    /**
     * [storeMembers description]
     * @param  ServerRequestInterface $request  [description]
     * @param  ResponseInterface      $response [description]
     * @return [type]                           [description]
     */
    public function storeMembers(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($errors = $this->validate($request, new AppointmentMemberRequest)) {
            return $errors;
        }

        $user = $this->auth->user();

        $patientresource = Patient::where('PatientID', $user->patient_id)->first();

        $data = $request->getParsedBody();

        $data['first_name'] = $patientresource['firstname'];
        $data['middle_name'] = $patientresource['middlename'];
        $data['last_name'] = $patientresource['lastname'];
        $data['birthday'] = $patientresource['birthday'];
        $data['email'] = $user->email_address;
        $data['gender'] = $patientresource['gender'];

        $finalDate = str_replace("/", "-", $data['start_date']);
        $preferredDate = $finalDate.' '.$data['start_time'];
        $displayDate = date("m-d-Y", strtotime($finalDate)).' at '.date("h:i:sa", strtotime($data['start_time']));

        $data['start_at'] = date("Y-m-d H:i", strtotime($preferredDate));

        $resource = $this
            ->appointmentRepository
            ->saveMembers($data);

        $title = 'Belo App - Appointment Request - BPC Member - '. $data['first_name'].' '.$data['last_name'];
        $email = $data['email'];
        $description = "{$data['branch']} {$displayDate}";
        $sender = "{$data['first_name']} {$data['last_name']}";
        $mobile = $data['contact_number'];
        $sex = $data['gender'];
        $origin = ($data['source'] == 1) ? 'IOS' : 'Android';
        $card_number = $user->card_number;

        try {
            $mail = new Mail();
            $resource = $mail->sendMemberAppointmentRequest($title, $data['notes'], $email, $description, $sender, $mobile, $sex, $origin, $card_number);
            return $this->jsonResponse(['message' => 'Message Sent']);
        } catch (Exception $e) {
            $mail->stopMail();

            return $this->errorInternalServerError($e->getMessage());
        }

        return $this->jsonResponse(['message' => 'Booking request sent']);
    }

    /**
     * [storeMembersRebook description]
     * @param  ServerRequestInterface $request  [description]
     * @param  ResponseInterface      $response [description]
     * @return [type]                           [description]
     */
    public function storeMembersRebook(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $id = $args['id'];
        $resource = $this->myscheduleRepository->findByAppointmentID($id);

        if ($resource) {

            if ($errors = $this->validate($request, new AppointmentMemberRequest)) {
                return $errors;
            }

            $user = $this->auth->user();

            $patientresource = Patient::where('PatientID', $user->patient_id)->first();

            $data = $request->getParsedBody();

            $data['first_name'] = $patientresource['first_name'];
            $data['middle_name'] = $patientresource['middle_name'];
            $data['last_name'] = $patientresource['last_name'];
            $data['birthday'] = $patientresource['birthday'];
            $data['email'] = $user['email_address'];
            $data['gender'] = $patientresource['gender'];
            $data['appointments_id'] = $id;

            $finalDate = str_replace("/", "-", $data['start_date']);
            $preferredDate = $finalDate.$data['start_time'];
            $data['start_at'] = date("Y-m-d H:i", strtotime($preferredDate));

            $resource = $this
                ->appointmentRepository
                ->saveMembers($data);

            return $this->jsonResponse(['message' => 'Booking request sent']);

        }

        return $this->errorNotFound();
    }

}