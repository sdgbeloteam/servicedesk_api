<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\ServiceMenu\ServiceMenu;
use ServiceDesk\ServiceMenu\ServiceMenuRepository;
use ServiceDesk\ServiceMenu\ServiceMenuTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ServiceMenuController extends Controller
{
	/**
	 * 
	 **/
	protected $serviceMenuRepository;
     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->serviceMenuRepository = new ServiceMenuRepository;
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $modelData = $this->serviceMenuRepository->findServicesByCategory($args['category-id']);

        if ($modelData) {
            return $this->collection($modelData, new ServiceMenuTransformer);
        }

        return $this->errorNotFound();
    }

    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $modelData = $this->serviceMenuRepository->findServicesById($args['service-id'], $args['category-id']);

        if ($modelData) {
            return $this->collection($modelData, new ServiceMenuTransformer);
        }

        return $this->errorNotFound();
    }

}
