<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\Loyalty\Loyalty;
use ServiceDesk\Loyalty\LoyaltyTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class LoyaltyController extends Controller
{
     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $user = $this->auth->user();

    	$patient = Loyalty::where('cardno', $user->card_number)
            ->first();

        if ($patient) {
            return $this->item(
                $patient, new LoyaltyTransformer
            );
        }

        return $this->errorNotFound();
    }

}
