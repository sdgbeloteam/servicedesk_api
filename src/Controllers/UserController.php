<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\User\User;
use ServiceDesk\Patient\Patient;
use ServiceDesk\Loyalty\Loyalty;
use ServiceDesk\User\UserRepository;
use ServiceDesk\Requests\UserSaveRequests;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UserController extends Controller
{
    /**
     * [$userRepository description]
     * @var [type]
     */
    public $userRepository;

     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->userRepository = new UserRepository;
    }

    public function signup(ServerRequestInterface $request, ResponseInterface $response)
    {
        if($this->validate($request, ['password' => 'required|min:6|max:25']))
        {
            return $this->errorBadRequest('The password must be at least 6 characters.');
        }

        if($this->validate($request, ['email' => 'required|email']))
        {
            return $this->errorBadRequest('Please check your email');
        }

        if($this->validate($request, ['card_number' => 'required|min:8|max:8']))
        {
            return $this->errorBadRequest('Card number must be the last 8 digit of your BPC');
        }

        $data = $request->getParsedBody();

        $patient =  Loyalty::where('cardno', $data['card_number'])->first();

        if($patient)
        {
            $patientBirthday = Patient::where('PatientID', $patient->PatientID)
            ->where('birthday', $data['birthday'])
            ->first();

            if($patientBirthday)
            {
                $modelData = $this->userRepository->verifyUserRegistration($data);

                if ($modelData == 'user') {
                    return $this->errorBadRequest('This account is already registered.');
                }
                else {
                    $modelRegister = $this->userRepository->saveRegistration($data, $patient->PatientID);

                    if($modelRegister == false) {
                        return $this->errorBadRequest('Email is already used.');
                    }

                    return $this->jsonResponse([
                        "message" => "You have successfully signed up " . $patientBirthday->firstname . " " . $patientBirthday->lastname . "! Please check your email (".$data['email'].") for activation of your account."
                    ]);
                }
            }

            return $this->errorBadRequest('Birthday is invalid.');
        }

        return $this->errorBadRequest('Loyalty card is invalid.');

    }

}
