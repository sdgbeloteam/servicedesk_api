<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\Branch\BranchRepository;
use ServiceDesk\Branch\Branch;
use ServiceDesk\Branch\BranchTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class BranchController extends Controller
{
    /**
     * [$medicalpersonnelRepository description]
     * @var [type]
     */
    protected $branchRepository;

    /**
     * Create an instance of MedicalPersonnelController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->branchRepository = new BranchRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->paginator(
            $this->branchRepository->paginate(
                20,
                $request->getQueryParam('page'),
                $request->getQueryParam('query')
        ), new BranchTransformer);

        return $this->errorNotFound();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $resource = $this->branchRepository->findByBranchId($args['id']);

        if ($resource) {
            return $this->item(
                $resource, new BranchTransformer
            );
        }

        return $this->errorNotFound();
    }


}