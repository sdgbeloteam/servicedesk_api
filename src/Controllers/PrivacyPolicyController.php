<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\PrivacyPolicy\PrivacyPolicy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class PrivacyPolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        $data = PrivacyPolicy::where('id', '1')->first();

        return $this->jsonResponse([
            'message' => $data['content'],
            'created_at' => $data['created_at']
        ]);
    }

}