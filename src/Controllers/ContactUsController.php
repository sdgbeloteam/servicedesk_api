<?php

namespace ServiceDesk\Controllers;

use Exception;
use ServiceDesk\Mail\Mail;
use ServiceDesk\Requests\ContactUsRequest;
use Psr\Http\Message\ResponseInterface;
use ServiceDesk\ContactUs\ContactUsRepository;
use Psr\Http\Message\ServerRequestInterface;

class ContactUsController extends Controller
{
    /**
     * [$contactusRepository description]
     * @var [type]
     */
    protected $contactusRepository;

    /**
     * [$mail description]
     * @var [type]
     */
    public $mail;

    /**
     * Create an instance of PatientController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->contactusRepository = new ContactUsRepository;

    }

     /**
     * [store description]
     * @param  ServerRequestInterface $request  [description]
     * @param  ResponseInterface      $response [description]
     * @return [type]                           [description]
     */
    public function store(ServerRequestInterface $request, ResponseInterface $response)
    {
        if ($errors = $this->validate($request, new ContactUsRequest)) {
            return $errors;
        }

        $data = $request->getParsedBody();

        $resource = $this
            ->contactusRepository
            ->save($data);

        $sender = "{$data['first_name']} {$data['last_name']}";
        $email = $data['email'];

        try {
            $mail = new Mail();
            $title = 'Belo App - Contact Us Support '.$sender;
            $description = 'nothing';
            $mobile = $data['mobile'];
            $resource = $mail->send($title, $data['content'], $email, $description, $sender, $mobile);
            return $this->jsonResponse(['message' => 'Message Sent']);
        } catch (Exception $e) {
            $mail->stopMail();

            return $this->errorInternalServerError($e->getMessage());
        }
    }

}
