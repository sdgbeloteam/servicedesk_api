<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\Version\Version;
use ServiceDesk\Version\VersionTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class VersionController extends Controller
{
     /**
     * Create an instance of VersionController
     */
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function android(ServerRequestInterface $request, ResponseInterface $response)
    {
        $modelData =  Version::where('id', 'android')->first();

        if($modelData)
        {
            return $this->item($modelData, new VersionTransformer);
        }

        return $this->errorNotFound();
    }

    public function ios(ServerRequestInterface $request, ResponseInterface $response)
    {
        $modelData =  Version::where('id', 'ios')->first();

        if($modelData)
        {
            return $this->item($modelData, new VersionTransformer);
        }

        return $this->errorNotFound();
    }

}
