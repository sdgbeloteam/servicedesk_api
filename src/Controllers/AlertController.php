<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\Alert\AlertRepository;
use ServiceDesk\Alert\AlertTransformer;
use ServiceDesk\Alert\AlertCountTransformer;
use ServiceDesk\Alert\Alert;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AlertController extends Controller
{
    /**
     * [$medicalpersonnelRepository description]
     * @var [type]
     */
    protected $alertRepository;

    /**
     * Create an instance of MedicalPersonnelController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->alertRepository = new AlertRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        $user = $this->auth->user();

        if($user) {
            $username = $user->email_address;

            return $this->collection(
                $this->alertRepository->findByUsername(
                    $username
                ),
                new AlertTransformer
            );
        }

        return $this->errorNotFound();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $id = $args['id'];
        $resource = $this->alertRepository->findByID($id);

        if ($resource) {
            $resource = $this
                ->alertRepository
                ->update($id);

            return $this->item(
                $resource, new AlertTransformer
            );
        }

        return $this->errorNotFound();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function showCount(ServerRequestInterface $request, ResponseInterface $response)
    {
        $user = $this->auth->user();

        $username = $user->email_address;

        $resource = $this->alertRepository->findCountByUsername($username);

        return $this->collection(
            $resource, new AlertCountTransformer
        );

    }
}