<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\User\User;
use ServiceDesk\User\UserPasswordRepository;
use ServiceDesk\User\UserRepository;
use ServiceDesk\Patient\Patient;
use ServiceDesk\Mail\Mail;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UserPasswordController extends Controller
{
    /**
     * [$userPasswordRepository description]
     * @var [type]
     */
    public $userPasswordRepository;

    public $userRepository;

     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->userPasswordRepository = new UserPasswordRepository;
        $this->userRepository = new UserRepository;
    }

    public function reset(ServerRequestInterface $request, ResponseInterface $response)
    {
        if($errors = $this->validate($request, ['email' => 'required|email']))
        {
            return $errors;
        }

        $data = $request->getParsedBody();
        $data['password'] = substr(md5(uniqid()), 0, 8);

        $patientData = $this->userPasswordRepository->findUserEmail($data);

        if($patientData)
        {
            $patientId = $this->userRepository->findByUserAccess($data['email']);

            $patientName = Patient::where('PatientID', $patientId->patient_id)->first();

            $sender = $patientName->firstname;
            $email = $data['email'];

            try {
                $mail = new Mail();
                $title = 'Belo App - Password Reset';
                $resource = $mail->sendPasswordReset($title, $email, $data['password'], $sender);
                return $this->jsonResponse(['message' => 'Message Sent']);
            } catch (Exception $e) {
                $mail->stopMail();

                return $this->errorInternalServerError($e->getMessage());
            }
        }

        return $this->jsonResponse([
                "message" => "This account was not validated. Please contact our Belo Helpdesk instead, call: +632 819 BELO (2356) – Philippines or +310 742 4843 – International"
                ]);
    }

    public function update(ServerRequestInterface $request, ResponseInterface $response)
    {
        $user = $this->auth->user();

        if ($errors = $this->validate($request, ['password' => 'required|min:6|max:25', 'old_password' => 'required'])) {
            return $errors;
        }

        if($request->getParsedBodyParam('old_password'))
        {
            if(!password_verify($request->getParsedBodyParam('old_password'), $user->password))
            {
                return $this->errorBadRequest('Invalid password.');
            }
        }

        $data = $request->getParsedBody();

        $patientData = $this->userPasswordRepository->resetPassword($user->id, $data['password']);

        if($patientData)
        {
            return $this->jsonResponse([
                "message" => "Password has been updated."
            ]);
        }

        return $this->errorBadRequest('Something went wrong.');
    }
}
