<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\MyTransaction\MyTransaction;
use ServiceDesk\MyTransaction\MyTransactionRepository;
use ServiceDesk\MyTransaction\MyTransactionTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class MyTransactionController extends Controller
{
	protected $myTransactionRepository;

     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->myTransactionRepository = new MyTransactionRepository;
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $user = $this->auth->user();

        $transaction = $this->myTransactionRepository->findMyTransaction($user->patient_id);

        if ($transaction) {
            return $this->collection(
                $transaction, new MyTransactionTransformer()
            );
        }

        return $this->errorNotFound();
    }

}
