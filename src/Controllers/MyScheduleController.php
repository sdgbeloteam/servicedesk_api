<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\MySchedule\MyScheduleRepository;
use ServiceDesk\MySchedule\MySchedule;
use ServiceDesk\MySchedule\MyScheduleTransformer;
use ServiceDesk\Requests\MyScheduleUpdateRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class MyScheduleController extends Controller
{
    /**
     * [$medicalpersonnelRepository description]
     * @var [type]
     */
    protected $myscheduleRepository;

    /**
     * Create an instance of MedicalPersonnelController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->myscheduleRepository = new MyScheduleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        $user = $this->auth->user();

        $patient_id = $user->patient_id;

        return $this->collection(
            $this->myscheduleRepository->findByPatientID(
                $patient_id
            ),
            new MyScheduleTransformer
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $resource = $this->myscheduleRepository->findByAppointmentID($args['id']);

        if ($resource) {
            return $this->item(
                $resource, new MyScheduleTransformer
            );
        }

        return $this->errorNotFound();
    }

    /**
     * updateAppointment the specified resource in storage.
     * @param  ServerRequestInterface $request  [description]
     * @param  ResponseInterface      $response [description]
     * @param  [type]                 $args     [description]
     * @return [type]                           [description]
     */
    public function updateAppointment(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $user = $this->auth->user();

        $username = $user->email_address;

        if($resource = $this->myscheduleRepository->findByAppointmentID($args['id'])) {

            $appointmentStatus = $resource->appointment_status;

            if($appointmentStatus == 'Completed' || $appointmentStatus == 'Cancelled') {
                return $this->errorBadRequest('Cannot update selected appointment');
            }

            $data['appointment_status'] = 'Cancelled';
            $data['user'] = $username;

            $resource = $this
                ->myscheduleRepository
                ->updateAppointmentStatus($data, $args['id']);

            return $this->item($resource, new MyScheduleTransformer);
        }

        return $this->errorNotFound();
    }

    /**
     * updateConfirmation the specified resource in storage.
     * @param  ServerRequestInterface $request  [description]
     * @param  ResponseInterface      $response [description]
     * @param  [type]                 $args     [description]
     * @return [type]                           [description]
     */
    public function updateConfirmation(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $user = $this->auth->user();

        $username = $user->email_address;

        if($resource = $this->myscheduleRepository->findByAppointmentID($args['id'])) {

            $appointmentStatus = $resource->appointment_status;
            $confirmationStatus = $resource->confirmation_status;

            if($appointmentStatus == 'Completed' || $appointmentStatus == 'Cancelled' || $confirmationStatus == 'Confirmed') {
                return $this->errorBadRequest('Cannot update selected appointment');
            }

            $data['confirmation_status'] = 'Confirmed';
            $data['user'] = $username;

            $resource = $this
                ->myscheduleRepository
                ->updateConfirmationStatus($data, $args['id']);

            return $this->item($resource, new MyScheduleTransformer);
        }

        return $this->errorNotFound();
    }


}