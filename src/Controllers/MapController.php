<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\Map\Map;
use ServiceDesk\Map\MapTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class MapController extends Controller
{
     /**
     * Create an instance of UserController
     */
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
    	$patient = Map::where('key_status', '1')
            ->first();

        if ($patient) {
            return $this->item(
                $patient, new MapTransformer
            );
        }

        return $this->errorNotFound();
    }

}
