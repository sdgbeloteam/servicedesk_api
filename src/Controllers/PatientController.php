<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\Patient\Patient;
use ServiceDesk\Patient\PatientRepository;
use ServiceDesk\Patient\PatientTransformer;
use ServiceDesk\Loyalty\Loyalty;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class PatientController extends Controller
{
    /**
     * [$patientRepository description]
     * @var [type]
     */
    public $patientRepository;

     /**
     * Create an instance of ProfileController
     */
    public function __construct($container)
    {
        parent::__construct($container);

        $this->patientRepository = new PatientRepository;
    }


    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
    	$user = $this->auth->user();

    	$patient = Patient::where('patientid', $user->patient_id)
            ->first();

        if ($patient) {
            return $this->item(
                $patient, new PatientTransformer
            );
        }

        return $this->errorNotFound();
    }

}
