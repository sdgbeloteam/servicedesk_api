<?php

namespace ServiceDesk\Controllers;

use ServiceDesk\SocialMedia\SocialMedia;
use ServiceDesk\SocialMedia\SocialMediaTransformer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class SocialMediaController extends Controller
{
    /**
     * Create an instance of MedicalPersonnelController
     */
    public function __construct($container)
    {
        parent::__construct($container);
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
    	$socialMedia =  SocialMedia::orderBy('social_media', 'asc')->get();

    	return $this->collection($socialMedia, new SocialMediaTransformer);
    }
}