<?php

return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        'url' => env('APP_URL', 'http://localhost:8000'),

        'jwt' => [
            'secret' => env('JWT_SECRET', 'CHANGE_ME_YOU_MONSTER'),
            'ttl' => env('JWT_TTL', 43200),
            'algo' => env('JWT_ALGO', 'HS256')
        ],

        // Language settings
        'lang' => [
            'locale' => 'en',
            'lang_path' => __DIR__ . '/../lang'
        ],

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        'intervention' => [
            'driver' => 'gd'
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // database
        'database' => [
            'driver'     => env('DB_DRIVER', 'mysql'),
            'host'       => env('DB_HOST', '127.0.0.1'),
            'port'       => env('DB_PORT', 3306),
            'database'   => env('DB_NAME', 'belo_database_test'),
            'username'   => env('DB_USER', 'root'),
            'password'   => env('DB_PASS', 'belo_test'),
            'charset'    => env('DB_CHARSET', 'utf8'),
            'collation'  => env('DB_COLLATION', 'utf8_unicode_ci'),
            'strict'     => false,
        ],

        // belo modules
        'services' => [
            'host' => env('STORAGE_HOST', 'http://219.90.86.130:33264'),
            'modules' => [
                'archives' => env('STORAGE_ARCHIVES', '/belo/module_nurses/uploaded_patient_pictures'),
                'faq' => env('STORAGE_FAQ', '/belo/assets/faq'),
                'services' => env('STORAGE_SERVICES', '/belo/assets/services'),
                'beforeAndAfter' => env('STORE_BEFORE_AND_AFTER', '/belo/assets/before-and-after'),
                'pxBeforeAndAFter' => env('STORAGE_PX_BEFORE_AND_AFTER', '/belo/module_nurses/patient-before-and-after'),
                'pxSignature' => env('STORAGE_PX_SIGNATURE', '/belo/module_nurses/patient-signature'),
                'pxProfilePicture' => env('STORAGE_PX_PROFILE_PICTURE', '/belo/module_nurses/patient-profile-picture'),
                'doctorProfilePicture' => env('STORAGE_DOCTOR_PROFILE_PICTURE', '/belo/module_nurses/doctor-profile-picture'),
                'promotion' => env('STORAGE_PROMOTIONS', '/belo/assets/promotions'),
                'stickers' => env('STORAGE_STICKERS', '/belo/assets/stickers'),
                'promotion' => env('STORAGE_PROMOTIONS', '/belo/assets/promotions'),
                'doctors' => env('STORAGE_DOCTORS', '/belo/assets/doctors'),
                'anatomies' => env('STORAGE_ANATOMIES', '/belo/assets/anatomies')
            ]
        ],
    ],

];
