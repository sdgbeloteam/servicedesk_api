<?php

namespace ServiceDesk\Uploader;

use Ramsey\Uuid\Uuid;
use Psr\Container\ContainerInterface;

class UploadedFile
{
    /**
     * [$manager description]
     * @var [type]
     */
    protected $manager;

    /**
     * [$encodedImage description]
     * @var [type]
     */
    protected $encodedImage;

    /**
     * [$uploadedFile description]
     * @var [type]
     */
    protected $filename;

    /**
     * [$storagePath description]
     * @var [type]
     */
    protected $storagePath;

    /**
     * [__construct description]
     * @param [type] $containaer   [description]
     * @param [type] $encodedImage [description]
     */
    public function __construct($manager, $encodedImage, $storagePath)
    {
        $this->manager = $manager;

        if ($manager instanceof ContainerInterface) {
            $this->manager = $this->getImageManager();
        }

        $this->storagePath = rtrim($storagePath, '/');
        $this->encodedImage = $encodedImage;
    }

    /**
     * [save description]
     * @return [type] [description]
     */
    public function save($name = null, $ext = 'jpg')
    {
        $this->filename = $this->generateFilename($ext);

        $image = $this->manager->make($this->getEncodedImage());

        $this->manager
            ->canvas($image->width(), $image->height(), '#ecf0f1')
            ->fill($image)
            ->save($this->storagePath.'/'. $this->filename);

        return $this;
    }

    /**
     * [getFileName description]
     * @return [type] [description]
     */
    protected function generateFilename($extension = null)
    {
        return Uuid::uuid4()->toString() . ($extension ? ".{$extension}" : null);
    }

    /**
     * Returns the encoded image
     *
     * @return [type] [description]
     */
    protected function getEncodedImage()
    {
        return $this->encodedImage;
    }

    /**
     * Returns the file name of the object
     *
     * @return [type] [description]
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Returns the default image manager
     *
     * @return [type] [description]
     */
    protected function getImageManager()
    {
        return $this->manager->get('image.manager');
    }
}
