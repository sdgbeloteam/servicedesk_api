<?php

namespace ServiceDesk\Mail;

use Swift_Message;
use Swift_Mailer;
use Swift_SmtpTransport;
use ServiceDesk\Faq\FaqController;
use Illuminate\Support\Facades\View;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Support\ServiceProvider;

class Mail
{
    protected $mailer;

    protected $transport;

    /**
     * [__construct description]
     */
    public function __construct()
    {
        //Setup the env for email login credentials
        $emailUsername = env('EMAIL_USERNAME', '');
        $emailPassword = env('EMAIL_PASSWORD', '');
        $emailPort = env('EMAIL_PORT', '');
        $emailSmtp = env('EMAIL_SMTP', '');
        $emailEncryption = env('EMAIL_ENCRYPTION', null);

        // Create the Transport
        $this->transport = $transport = (new Swift_SmtpTransport($emailSmtp, $emailPort))
          ->setUsername($emailUsername)
          ->setPassword($emailPassword)
          ->setEncryption($emailEncryption);

        // Create the Mailer using your created Transport
        $this->mailer = new Swift_Mailer($transport);
    }

    /**
     * Get switf mailer instance
     * @return [type] [description]
     */
    public function getMailerInstance()
    {
        return $this->mailer;
    }

    /**
     * Interrupt the mail being sent
     * @return [type] [description]
     */
    public function stopMail()
    {
        return $this->transport->stop();
    }

    /**
     * [send description]
     * @param  [type] $title [title of the email]
     * @param  [type] $content [additional text coming from the user]
     * @param  [type] $email [email address of the recepient]
     * @param  [type] $description [full description of the faq]
     * @param  [type] $sender [full name of the sender]
     * @return [type]           [description]
     */
    public function send($title, $content, $email, $description, $sender, $mobile)
    {
        //Search for the placeholders to be replaced
        $variables = array('email', 'description', 'content', 'sender', 'mobile');
        $replacers = array($email, $description, $content, $sender, $mobile);

        //Get the file contents and replace the variables with the replacers
        $url = file_get_contents(__DIR__ .DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'faq.php');
        $body = str_replace($variables, $replacers, $url);

        // Create a message
        $message = (new Swift_Message($title))
          ->setFrom(['info8@belomed.com' => 'Belo Medical Group'])
          ->setBody($body,'text/html');

        $message->setTo(array('info@belomed.com','jgayod@belomed.com'));
        $result = $this->mailer->send($message);
    }

    /**
     * [send description]
     * @param  [type] $title [title of the email]
     * @param  [type] $content [additional text coming from the user]
     * @param  [type] $email [email address of the recepient]
     * @param  [type] $description [full description of the faq]
     * @param  [type] $sender [full name of the sender]
     * @return [type]           [description]
     */
    public function sendAppointmentRequest($title, $content, $email, $description, $sender, $mobile, $sex, $origin)
    {
        //Search for the placeholders to be replaced
        $variables = array('email', 'description', 'content', 'sender', 'mobile', 'sex', 'origin');
        $replacers = array($email, $description, $content, $sender, $mobile, $sex, $origin);

        //Get the file contents and replace the variables with the replacers
        $url = file_get_contents(__DIR__ .DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'appointment-request.php');
        $body = str_replace($variables, $replacers, $url);

        // Create a message
        $message = (new Swift_Message($title))
          ->setFrom(['info8@belomed.com' => 'Belo Medical Group'])
          ->setBody($body,'text/html');

        $message->setTo(array('info@belomed.com','jgayod@belomed.com'));
        $result = $this->mailer->send($message);
    }

     /**
     * [send description]
     * @param  [type] $title [title of the email]
     * @param  [type] $content [additional text coming from the user]
     * @param  [type] $email [email address of the recepient]
     * @param  [type] $description [full description of the faq]
     * @param  [type] $sender [full name of the sender]
     * @return [type]           [description]
     */
    public function sendMemberAppointmentRequest($title, $content, $email, $description, $sender, $mobile, $sex, $origin, $card_number)
    {
        //Search for the placeholders to be replaced
        $variables = array('email', 'description', 'content', 'sender', 'mobile', 'sex', 'origin', 'card_number');
        $replacers = array($email, $description, $content, $sender, $mobile, $sex, $origin, $card_number);

        //Get the file contents and replace the variables with the replacers
        $url = file_get_contents(__DIR__ .DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'appointment-request-member.php');
        $body = str_replace($variables, $replacers, $url);

        // Create a message
        $message = (new Swift_Message($title))
          ->setFrom(['info8@belomed.com' => 'Belo Medical Group'])
          ->setBody($body,'text/html');

        $message->setTo(array('info@belomed.com','jgayod@belomed.com'));
        $result = $this->mailer->send($message);
    }

    /**
     * [send description]
     * @param  [type] $title [title of the email]
     * @param  [type] $email [email address of the recepient]
     * @param  [type] $passcode [passcode of the temporary password]
     * @param  [type] $sender [full name of the sender]
     */
    public function sendPasswordReset($title, $email, $passcode, $sender)
    {
        //Search for the placeholders to be replaced
        $variables = array('email', 'passcode', 'sender');
        $replacers = array($email, $passcode, $sender);

        //Get the file contents and replace the variables with the replacers
        $url = file_get_contents(__DIR__ .DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.'forgot-password.php');
        $body = str_replace($variables, $replacers, $url);

        // Create a message
        $message = (new Swift_Message($title))
          ->setFrom(['info8@belomed.com' => 'Belo Medical Group'])
          ->setBody($body,'text/html');

        $message->setTo(array($email));
        $result = $this->mailer->send($message);
    }

}
