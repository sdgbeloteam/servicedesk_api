<?php

$app->group('/api', function() {
    // Login
    $this->post('/auth/login', 'ServiceDesk\Controllers\AuthController:login');
});

$app->get('/attachments', 'ServiceDesk\Controllers\AttachmentController:show');
$app->get('/ping', 'ServiceDesk\Controllers\PingController:ping');
