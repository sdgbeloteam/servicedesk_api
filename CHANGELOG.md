## 0.1.3

* Added my-schedule route
* Created my-schedule updating of Appointment or Confirmation status

## 0.1.2

* Added Mail function for Contact Us
* Draft template created for Helpdesk

## 0.1.1

* Added branches route

## 0.1

* Added Filters and Query for MedicalPersonnel
* Updated API version to base version 0.1.0
* Added new routes for showing Medical Personnel
* Added Pagination meta for Medical Personnel
